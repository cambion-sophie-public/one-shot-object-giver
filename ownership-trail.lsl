// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
// Make sure this script is TRANSFER 
// before putting in the unique item.
// Otherwise you'll end up with
// a no-transfer no-copy item.
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// URL of the Discord webhook to talk to.
// Copy this from Discord.
string discordWebhookURL = "https://discordapp.com/api/webhooks/1234567890/alskdjfalskdjfaklsjdflaksdjflkasjdfkjsadflkjsaldkjfs";

notifyViaDiscord()
{
    // A plain text message to a Discord webhook
    // needs to be packaged in JSON with the form
    // {"content": "Text goes here."}
    string message =
        "Object " + llGetObjectName() + 
        " is owned by by " + llGetDisplayName(llGetOwner()) + " (" + llGetUsername(llGetOwner()) + ") " +
        "UUID: " + (string)llGetOwner() + ".";
    string json = llList2Json(JSON_OBJECT, ["content", message]);
    llHTTPRequest(
        discordWebhookURL, 
        [
            HTTP_METHOD, "POST",
            HTTP_MIMETYPE, "application/json;charset=utf-8",
            HTTP_BODY_MAXLENGTH, 16384
        ],
        json
    );
}
default
{
    changed( integer change )
    {
        if(change & CHANGED_OWNER) llResetScript();
    }

    state_entry()
    {
        notifyViaDiscord();
    }
}
