# Unique Items

SL was not designed to give out one-of-a-kind objects.
This is a quick and dirty attempt to deal with the challenges. 

## One-Shot Giver

Due to the limitations of SL, 
there is no way to give an object to an avatar and 
receive confirmation the object was actually accepted into inventory. 

These steps would give a record of who got the object in case of "oops," 
including deleting from inventory after receiving the object.

* When touched, remember the toucher. Ignore all touchers after this. 
* Send a message to a Discord channel to say toucher is first person.
* Pause briefly.
* Delete the remaining from inventory.

## Trail of Ownership (Optional)

If a person gave the item to another person, and that other person deleted it from their inventory or otherwise lost it, there is no way to prove the transfer.

To mitigate this, it's possible to have a small script in the object.

* When an object is transferred, all scripts are reset. (This is not true of person-to-person transfers.)
* If the item is rezzed, the script knows it's the first time to run with this owner.
* The script could report ownership to a Discord channel. This would provide a record of ownership, then go silent.
  * This only works if the item is transferred, and rezzed out. 
* This script could delete itself after a certain day so it's not spamming a Discord channel after the event.
