// Who touched first.
key toucher = NULL_KEY;

// Name of the item in inventory.
// This must be exact.
// If it's not, it will let you know.
string inventory = "New Note";

// How long (in seconds) to give the receiver to accept the inventory 
// before deleting the inventory.
float delayBeforeDeleting = 120;

// URL of the Discord webhook to talk to.
// Copy this from Discord.
string discordWebhookURL = "https://discordapp.com/api/webhooks/766887394380349000/_______UhwQK0GpVJTwO1KIpW-2NJKlenu7UKfVF3d8KFTTqz4pQ8Al9O4gDEDHyMN7U";

notifyViaDiscord()
{
    // A plain text message to a Discord webhook
    // needs to be packaged in JSON with the form
    // {"content": "Text goes here."}
    string message =
        "Object " + inventory + 
        " was found by " + llGetDisplayName(toucher) + " (" + llGetUsername(toucher) + ") " +
        "UUID: " + (string)toucher + ".";
    string json = llList2Json(JSON_OBJECT, ["content", message]);
    llHTTPRequest(
        discordWebhookURL, 
        [
            HTTP_METHOD, "POST",
            HTTP_MIMETYPE, "application/json;charset=utf-8",
            HTTP_BODY_MAXLENGTH, 16384
        ],
        json
    );
}

/**
 * Display the name of an agent in a way that can be clicked on.
 */
string urlName(key agent)
{
    if(agent)
        return "secondlife:///app/agent/" + (string)agent + "/inspect";
    else
        return "[no agent key given]";
}

default
{
    http_response( key request_id, integer status, list metadata, string body )
    {
        // Successful Discord post.
        if(llFloor(status / 100) == 2) {
            llGiveInventory(toucher, inventory);
            llSleep(delayBeforeDeleting);
            llRemoveInventory(inventory);
        } else {
            llSay(0, "Something went wrong trying to announce your find. Please contact the event organizers.");
        }
    }

    state_entry()
    {
        if(llGetInventoryType(inventory) == INVENTORY_NONE) {
            llSay(0, "The object to give (" + inventory + ") is missing. Please add it.");
        } else {
            llSay(0, "Ready.");
        }
    }

    touch_start(integer total_number)
    {
        if(toucher) {
            llSay(0, "This item has already been claimed by " + urlName(toucher) + ".");
            return;
        }

        if(llGetInventoryType(inventory) == INVENTORY_NONE) {
            llSay(0, "The object to give (" + inventory + ") is missing.");
            return;
        }

        toucher = llDetectedKey(0);
        llSay(0, "Congratulations, " + urlName(toucher) + " you found an item!");

        llSay(0, "Attempting to announce your find....");
        notifyViaDiscord();
    }
}
